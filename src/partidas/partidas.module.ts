import { Module } from '@nestjs/common';
import { PartidasService } from './partidas.service';
import { PartidasController } from './partidas.controller';
import { Partida } from './entities/partida.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [PartidasController],
  providers: [PartidasService],
  imports: [
    TypeOrmModule.forFeature([Partida])
  ]
})
export class PartidasModule {}
