import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Partida {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    nombre: string;

    @Column('text')
    apellido: string;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaNacimiento: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaBautismo: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaBautismoNovia: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaConfirmacion: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaDefuncion: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaSepultura: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaMatrimonio: Date;

    @Column({
        type: 'text',
        nullable: true
    })
    identificacion: string;

    @Column({
        type: 'text',
        nullable: true
    })
    edad: string;

    @Column({
        type: 'text',
        nullable: true
    })
    sexo: string;

    @Column({
        type: 'text',
        nullable: true
    })
    lugarNacimiento: string;

    @Column({
        type: 'text',
        nullable: true
    })
    filiacion: string;

    @Column({
        type: 'text',
        nullable: true
    })
    testigos: string;

    @Column({
        type: 'text',
        nullable: true
    })
    estadoCivil: string;

    @Column({
        type: 'text',
        nullable: true
    })
    padres: string;

    @Column({
        type: 'text',
        nullable: true
    })
    parroquiaBautismo: string;

    @Column({
        type: 'text',
        nullable: true
    })
    dispensas: string;

    @Column({
        type: 'text',
        nullable: true
    })
    nihilObstat: string;

    @Column({
        type: 'text',
        nullable: true
    })
    expediente: string;

    @Column({
        type: 'text',
        nullable: true
    })
    nombrePadre: string;

    @Column({
        type: 'text',
        nullable: true
    })
    nombreMadre: string;

    @Column({
        type: 'text',
        nullable: true
    })
    abuelosPaternos: string;

    @Column({
        type: 'text',
        nullable: true
    })
    abuelosMaternos: string;

    @Column({
        type: 'text',
        nullable: true
    })
    padrinos: string;

    @Column({
        type: 'text',
        nullable: true
    })
    autorizacion: string;

    @Column({
        type: 'text',
        nullable: true
    })
    ministro: string;

    @Column({
        type: 'text',
        nullable: true
    })
    fe: string;

    @Column({
        type: 'text',
        nullable: true
    })
    parroquia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    parroquiaConfirmacion: string;

    @Column({
        type: 'text',
        nullable: true
    })
    sacramentos: string;

    @Column({
        type: 'text',
        nullable: true
    })
    notaMarginal: string;

    @Column({
        type: 'text',
        nullable: true
    })
    libro: number;

    @Column({
        type: 'text',
        nullable: true
    })
    folio: number;

    @Column('text')
    numero: number;

    @Column({
        type: 'text',
        nullable: true
    })
    libroNovio: number;

    @Column({
        type: 'text',
        nullable: true
    })
    folioNovio: number;

    @Column({
        type: 'text',
        nullable: true
    })
    numeroNovio: number;

    @Column({
        type: 'text',
        nullable: true
    })
    libroNovia: number;

    @Column({
        type: 'text',
        nullable: true
    })
    folioNovia: number;

    @Column({
        type: 'text',
        nullable: true
    })
    numeroNovia: number;

    @Column({
        type: 'boolean',
        nullable: true
    })
    reposicion: boolean;

    @Column({
        type: 'text',
        nullable: true
    })
    tipoPartida: string;

    @Column({
        type: 'text',
        nullable: true
    })
    nombreNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    apellidoNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaNacimientoNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    estadoCivilNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    lugarNacimientoNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    padresNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    parroquiaBautismoNovia: string;

    @Column({
        type: 'text',
        nullable: true
    })
    nombreNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    apellidoNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    fechaNacimientoNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    estadoCivilNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    lugarNacimientoNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    padresNovio: string;

    @Column({
        type: 'text',
        nullable: true
    })
    parroquiaBautismoNovio: string;


    @Column({
        type: 'text',
        nullable: true
    })
    tipoMuerte: string;

    
    @Column({
        type: 'text',
        nullable: true
    })
    causaMuerte: string;

    
    @Column({
        type: 'text',
        nullable: true
    })
    lugarMuerte: string;

}
