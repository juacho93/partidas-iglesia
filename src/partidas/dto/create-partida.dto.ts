import { Type } from "class-transformer";
import { IsString, IsBoolean, IsDate, IsNumber, IsOptional, IsPositive, MinLength } from "class-validator";

export class CreatePartidaDto {

    @IsString()
    @MinLength(1)
    nombre: string;

    @IsString()
    @MinLength(1)
    apellido: string;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaNacimiento: Date;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaBautismo?: Date;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaConfirmacion?: Date;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaDefuncion?: Date;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaSepultura?: Date;

    @Type(() => Date)
    @IsDate()
    @IsOptional()
    fechaMatrimonio?: Date;
    
    @IsString()
    @IsOptional()
    identificacion?: string;

    @IsNumber()
    @IsOptional()
    edad?: string;

    @IsString()
    @IsOptional()
    sexo?: string;

    @IsString()
    @IsOptional()
    lugarNacimiento?: string;

    @IsString()
    @IsOptional()
    filiacion?: string;

    @IsString()
    @IsOptional()
    testigos?: string;

    @IsString()
    @IsOptional()
    estadoCivil?: string;

    @IsString()
    @IsOptional()
    padres?: string;

    @IsString()
    @IsOptional()
    parroquiaBautismo?: string;

    @IsString()
    @IsOptional()
    dispensas?: string;

    @IsString()
    @IsOptional()
    nihilObstat?: string;

    @IsString()
    @IsOptional()
    expediente?: string;

    @IsString()
    @IsOptional()
    nombrePadre?: string;

    @IsString()
    @IsOptional()
    nombreMadre?: string;

    @IsString()
    @IsOptional()
    abuelosPaternos?: string;

    @IsString()
    @IsOptional()
    abuelosMaternos?: string;

    @IsString()
    @IsOptional()
    padrinos?: string;

    @IsString()
    @IsOptional()
    autorizacion?: string;

    @IsString()
    @IsOptional()
    ministro?: string;

    @IsString()
    @IsOptional()
    fe?: string;

    @IsString()
    @IsOptional()
    parroquia?: string;

    @IsString()
    @IsOptional()
    parroquiaConfirmacion?: string;

    @IsString()
    @IsOptional()
    sacramentos?: string;

    @IsString()
    @IsOptional()
    notaMarginal?: string;

    @IsNumber()
    @IsOptional()
    @IsPositive()
    libro?: number;

    @IsNumber()
    @IsOptional()
    @IsPositive()
    folio: number;

    @IsNumber()
    @IsPositive()
    numero: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    libroNovio?: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    folioNovio?: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    numeroNovio?: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    libroNovia?: number;

    @IsNumber()
    @IsPositive()
    @IsOptional()
    folioNovia?: number;
    
    @IsNumber()
    @IsPositive()
    @IsOptional()
    numeroNovia?: number;

    @IsBoolean()
    @IsOptional()
    reposicion?: boolean;

    @IsOptional()
    @IsString()
    tipoPartida?: string;

    @IsOptional()
    @IsString()
    nombreNovia: string;

    @IsOptional()
    @IsString()
    apellidoNovia: string;

    @IsOptional()
    @IsString()
    fechaNacimientoNovia: string;

    @IsOptional()
    @IsString()
    estadoCivilNovia: string;

    @IsOptional()
    @IsString()
    lugarNacimientoNovia: string;

    @IsOptional()
    @IsString()
    padresNovia: string;

    @IsOptional()
    @IsString()
    parroquiaBautismoNovia: string;

    @IsOptional()
    @IsString()
    nombreNovio: string;

    @IsOptional()
    @IsString()
    apellidoNovio: string;

    @IsOptional()
    @IsString()
    fechaNacimientoNovio: string;

    @IsOptional()
    @IsString()
    estadoCivilNovio: string;

    @IsOptional()
    @IsString()
    lugarNacimientoNovio: string;

    @IsOptional()
    @IsString()
    padresNovio: string;

    @IsOptional()
    @IsString()
    parroquiaBautismoNovio: string;

    @IsOptional()
    @IsString()
    fechaBautismoNovia: string;

    @IsOptional()
    @IsString()
    tipoMuerte: string;

    @IsOptional()
    @IsString()
    causaMuerte: string;


    @IsOptional()
    @IsString()
    lugarMuerte: string;


}
