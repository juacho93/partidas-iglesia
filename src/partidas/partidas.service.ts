import { BadRequestException, Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePartidaDto } from './dto/create-partida.dto';
import { UpdatePartidaDto } from './dto/update-partida.dto';
import { Partida } from './entities/partida.entity';
import {validate as isUUID } from 'uuid'
import { NotFoundException } from '@nestjs/common/exceptions';
import { PaginationDto } from 'src/common/dtos/pagination.dto';

@Injectable()
export class PartidasService {

  private readonly logger = new Logger('PartidasService')

  /**
   * 
   * @param partidasRepository PATRON REPOSITORIO
   */
  constructor(
    @InjectRepository(Partida)
    private readonly partidasRepository: Repository<Partida>,
  ){

  }

 async create(createPartidaDto: CreatePartidaDto) {
        
    try {

      const validarPartida = await this.partidasRepository.findOne({
        where: {
          tipoPartida: createPartidaDto.tipoPartida,
          numero: createPartidaDto.numero
        },
      })

      if (validarPartida) {
        throw new NotFoundException(`Este número de partida ya esta en uso`);
      }

      const partida = this.partidasRepository.create(createPartidaDto);
      await this.partidasRepository.save(partida);
      return partida;
    } catch (error) {
      this.errorServer(error);
    }

  }

  findAll( paginationDto: PaginationDto) {
    
    const {limite = 10, offset = 0} = paginationDto;

    return this.partidasRepository.find({
      take: limite,
      skip: offset
    });
  }
  

  async findOne(term: string) {

    let partida: Partida[];

     if (isUUID(term)) {
       partida = [await this.partidasRepository.findOneBy({id: term})]
     } else {
      const queryBuilder = this.partidasRepository.createQueryBuilder();
      partida = await queryBuilder
      .where('UPPER(nombre) =:nombre or UPPER(apellido) =:apellido',{
        nombre: term.toUpperCase(),
        apellido: term.toUpperCase()
      }).getMany();
     }
    
     if (!partida) 
     throw new NotFoundException(`La partida con el identificador ${term} no fue encontrado`);

     return partida;
  }



  async update(id: string, updatePartidaDto: UpdatePartidaDto) {
    const partida = await this.partidasRepository.preload({
      id:id,
      ...updatePartidaDto
    });

    if (!partida) throw new NotFoundException(` La partida con el id: ${id} no fue encontrada`);

    try {
      const validarPartida = await this.partidasRepository.findOne({
        where: {
          tipoPartida: updatePartidaDto.tipoPartida,
          numero: updatePartidaDto.numero
        },
      })

      if (validarPartida) {
        if (validarPartida.id != partida.id) {
          throw new NotFoundException(`Este número de partida ya esta en uso`);
        }
      }

      await this.partidasRepository.save(partida);
      return partida;
      
    } catch (error) {
      this.errorServer(error);
    }

    
  }

  async remove(id: string) {
    const partida = await this.findOne(id)
    return await this.partidasRepository.delete(id);
  }
  

  /**
   * Funcion para detectar el error
   * @param error 
   */
  errorServer(error: any){
    if (error) 
    throw new BadRequestException(error)
    

    if (error.code === '23505') 
    throw new BadRequestException(error.detail)
    this.logger.error(error)
    throw new InternalServerErrorException('Error inesperado, verifique los logs del servidor');
  }

}
